(load "~/.sbclrc")
(ql:quickload :lass)

(setf filename "simple.css")

(setf font-stack
      "Georgia, \"Bitstream Charter\", \"Charis SIL\", Utopia, \"URW Bookman L\", serif")
(setf black     "#000000")
(setf grey      "#404040")
(setf lightgrey "#707070")
(setf white     "#ffffff")
(setf screen-breakpoint "576px")

(setf site-theme
      `((html
	 :position relative
	 :min-height "100%"
	 :margin 0
	 :padding 0)
	(body
	 :margin 0
	 :padding 0
	 :color ,grey
	 :background-color ,white
	 :font-family ,font-stack)
	(h1
	 :font-size 2.25em
	 :color ,black
	 :text-align center)
	(h2
	 :font-size 1.5em
	 :color ,black)
	((h2 a)
	 :color ,black)
	(p
	 :text-indent 40px)
	(a
	 :text-decoration none
	 :color ,lightgrey)
	((:and a :hover)
	 :color ,grey)
	(hr
	 :border-style solid
	 :border-width 0.5px)
	(((:or .summary .emails .links) ul)
	 :list-style none
	 :text-align center
	 :padding 0)
	(((:or .summary .emails .links) li)
	 :text-align center)
	(((:or .emails .links) li)
	 :display inline
	 :padding 0 5px)
	(.post-list
	 :list-style none)
	(.footer
	 :position absolute
	 :bottom 0
	 :width "100%"
	 :height 1em
	 :text-align center)))

(setf css-grid
      `((.container
	 :max-width 600px
	 :margin 0 auto
	 :padding-bottom 1.5em)
	(:media "(max-width: 640px)"
	 (.container
	  :margin 0 20px))
	(:media "(min-width: 576px)"
	 (.row
	  :display grid
	  :grid-template-columns "repeat(1, 1fr)"
	  :grid-gap 30px)
	 (.col-1
	  :grid-column "span 1")
	 (.col-2
	  :grid-column "span 2")
	 (.col-3
	  :grid-column "span 3")
	 (.col-4
	  :grid-column "span 4")
	 (.col-5
	  :grid-column "span 5")
	 (.col-6
	  :grid-column "span 6")
	 (.col-7
	  :grid-column "span 7")
	 (.col-8
	  :grid-column "span 8")
	 (.col-9
	  :grid-column "span 9")
	 (.col-10
	  :grid-column "span 10")
	 (.col-11
	  :grid-column "span 11")
	 (.col-12
	  :grid-column "span 12"))))


(defun make-css ()
  (apply #'lass:compile-and-write
	 `(,@site-theme ,@css-grid)))

(with-open-file (out filename
		     :direction :output
		     :if-exists :supersede)
		(format out (make-css)))

